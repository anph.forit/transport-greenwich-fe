$(document).on("submit", "#createForm", btnCreateSubmit);
function btnCreateSubmit(e) {
    e.preventDefault();
    const name = document.getElementById("txtStationName").value;
    const des  = document.getElementById("txtDescription").value;
    const loc = document.getElementById("sLocation").value;
    var token = localStorage.getItem("token");
    var host_url = localStorage.getItem("host_url");
    axios({
        method: "POST",
        url: host_url +"/station",
        headers: {
            'Authorization': "Bearer " + token,
            'Accept': 'application/json'
        },
        data: {
            station_name: name,
            description: des,
            loc_id: loc
        }
    })
        .then(function (res) {
            alert("Create successfully.")
            location.reload();
        })
        .catch(function (error) {
            console.log(error);
            alert("Error: " + error);
        });

    $("#createForm").validate(
        { submitHandler: function(form) {} }
    );
}

function changeActive(obj, id) {
    var token = localStorage.getItem("token");
    var host_url = localStorage.getItem("host_url");
    axios({
        method: "PUT",
        url: host_url +"/station",
        headers: {
            'Authorization': "Bearer " + token,
            'Accept': 'application/json'
        },
        data: {
            id: id,
            active: obj.checked
        }
    })
        .then(function (res) {
        })
        .catch(function (error) {
            console.log(error);
            alert("Error: " + error);
        });
}

function setEditData(id) {
    var token = localStorage.getItem("token");
    var host_url = localStorage.getItem("host_url");
    axios({
        method: "GET",
        url: host_url +"/searchstation",
        headers: {
            'Authorization': "Bearer " + token,
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        params: {
            id: id
        }
    })
        .then(function (res) {
            if (res.data.length > 0){
                document.getElementById("txtEditStationName").value = res.data[0].station_name;
                document.getElementById("txtEditDescription").value = res.data[0].description;
                document.getElementById("sEditLocation").value = res.data[0].loc_id;
                document.getElementById("txtId").value = id;
            }            
        })
        .catch(function (error) {
            console.log(error);
            alert("Error: " + error);
        });
}

$(document).on("submit", "#editForm", btnEditSubmit);
function btnEditSubmit(e) {
    e.preventDefault();
    const name = document.getElementById("txtEditStationName").value;
    const des  = document.getElementById("txtEditDescription").value;
    const loc = document.getElementById("sEditLocation").value;
    const id = document.getElementById("txtId").value;
    var token = localStorage.getItem("token");
    var host_url = localStorage.getItem("host_url");
    axios({
        method: "PUT",
        url: host_url +"/station",
        headers: {
            'Authorization': "Bearer " + token,
            'Accept': 'application/json'
        },
        data: {
            station_name: name,
            description: des,
            loc_id: loc,
            id: id
        }
    })
        .then(function (res) {
            alert("Update successfully.")
            location.reload();
        })
        .catch(function (error) {
            console.log(error);
            alert("Error: " + error);
        });

    $("#editForm").validate(
        { submitHandler: function(form) {} }
    );
}

$(document).ready(function() {
    var token = localStorage.getItem("token");
    var host_url = localStorage.getItem("host_url");
    axios({
        method: "GET",
        url: host_url +"/location",
        headers: {
            'Authorization': "Bearer " + token,
            'Accept': 'application/json'
        }
    })
        .then(function (res) {
            var lst = res.data.data;
            var content = '<option value="" selected>Choose Station</option>';
            for (var i = 0; i < lst.length; i++) {
                if (lst[i].active == true){
                    content += ` 
                    <option value="${lst[i].id}">${lst[i].loc_name}</option>                        
                    `;
                }                 
            }
            document.getElementById("sLocation").innerHTML = content;
            document.getElementById("sEditLocation").innerHTML = content;

            axios({
                method: "GET",
                url: host_url +"/station",
                headers: {
                    'Authorization': "Bearer " + token,
                    'Accept': 'application/json'
                }
            })
                .then(function (res) {
                    var lst = res.data.data;                    
                    var content = "";
                    for (var i = 0; i < lst.length; i++) {
                        if (lst[i].active == true){
                            content += ` 
                            <tr>
                                <td>${lst[i].station_name}</td>
                                <td>${lst[i].location.loc_name}</td>
                                <td>${lst[i].description}</td>
                                <td><label class="switch"><input type="checkbox" onchange="changeActive(this, ${lst[i].id})" checked><span class="slider round"></span></label></td>
                                <td>
                                    <a href="#" onclick="setEditData(${lst[i].id})" class="btn btn-success btn-circle btn-sm" title="Edit" data-toggle="modal" data-target="#editModal"><i class="fas fa-arrow-alt-circle-right"></i></a>
                                </td>
                            </tr>
                            `;
                        } else {
                            content += ` 
                            <tr>
                                <td>${lst[i].station_name}</td>
                                <td>${lst[i].location.loc_name}</td>
                                <td>${lst[i].description}</td>
                                <td><label class="switch"><input type="checkbox" onchange="changeActive(this, ${lst[i].id})" ><span class="slider round"></span></label></td>
                                <td>
                                    <a href="#" onclick="setEditData(${lst[i].id})" class="btn btn-success btn-circle btn-sm" title ="Edit" data-toggle="modal" data-target="#editModal"><i class="fas fa-arrow-alt-circle-right"></i></a>
                                </td>
                            </tr>
                            `;
                        }
                        
                    }
                    document.getElementById("staList").innerHTML = content;
                    var dtable = $('#dataTable').DataTable();
                    document.getElementById("dataTable_filter").style.display = "none";
                })
                .catch(function (error) {
                    console.log(error);
                    alert("Maybe your session is expired. You should login again.");
                });
        })
        .catch(function (error) {
            console.log(error);
            alert("Maybe your session is expired. You should login again.");
        });
});
