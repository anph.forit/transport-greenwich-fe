function setEditData(id) {
    var token = localStorage.getItem("token");
    var host_url = localStorage.getItem("host_url");
    axios({
        method: "GET",
        url: host_url + "/ticket/find_ticket",
        headers: {
            'Authorization': "Bearer " + token,
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        params: {
            id: id
        }
    })
        .then(function (res) {
            console.log(res.data);
            if (res.data.length > 0) {
                document.getElementById("txtUserName").value = res.data[0].user.name;
                document.getElementById("sEditBoat").value = res.data[0].schedule.boat_id;
                document.getElementById("sEditDeparture").value = res.data[0].schedule.departure_station_id;
                document.getElementById("sEditArrival").value = res.data[0].schedule.arrival_station_id;
                document.getElementById("txtEditTime").value = res.data[0].timeval;
                document.getElementById("txtEditSeat").value = res.data[0].seat;
                document.getElementById("txtEditDate").value = res.data[0].pick_date;
                document.getElementById("txtEditPayMethod").value = res.data[0].payment_method;
                document.getElementById("sEditPayStatus").value = res.data[0].payment_status;
                document.getElementById("sEditStatus").value = res.data[0].status;
                document.getElementById("txtEditPrice").value = res.data[0].price;
                document.getElementById("txtId").value = id;
            }
        })
        .catch(function (error) {
            console.log(error);
            alert("Error: " + error);
        });
}

$(document).on("submit", "#editForm", btnEditSubmit);
function btnEditSubmit(e) {
    e.preventDefault();
    const paystatus = document.getElementById("sEditPayStatus").value;
    const status = document.getElementById("sEditStatus").value;
    const price = document.getElementById("txtEditPrice").value;
    const id = document.getElementById("txtId").value;
    var token = localStorage.getItem("token");
    var host_url = localStorage.getItem("host_url");
    axios({
        method: "PUT",
        url: host_url + "/ticket/update_ticket",
        headers: {
            'Authorization': "Bearer " + token,
            'Accept': 'application/json'
        },
        data: {
            payment_status: paystatus,
            status: status,
            price: price,
            id: id
        }
    })
        .then(function (res) {
            alert("Update successfully.")
            location.reload();
        })
        .catch(function (error) {
            console.log(error);
            alert("Error: " + error);
        });

    $("#editForm").validate(
        { submitHandler: function (form) { } }
    );
}

$(document).ready(function () {
    var token = localStorage.getItem("token");
    var host_url = localStorage.getItem("host_url");
    axios({
        method: "GET",
        url: host_url + "/station",
        headers: {
            'Authorization': "Bearer " + token,
            'Accept': 'application/json'
        }
    })
        .then(function (res) {
            var lst = res.data.data;
            var content = '';
            for (var i = 0; i < lst.length; i++) {
                content += ` 
                    <option value="${lst[i].id}">${lst[i].station_name + " - " + lst[i].location.loc_name}</option>                        
                    `;
            }
            document.getElementById("sEditDeparture").innerHTML = content;
            document.getElementById("sEditArrival").innerHTML = content;
            axios({
                method: "GET",
                url: host_url + "/boat/get_boat",
                headers: {
                    'Authorization': "Bearer " + token,
                    'Accept': 'application/json'
                }
            })
                .then(function (res) {
                    var lst = res.data.data;
                    var content = '';
                    for (var i = 0; i < lst.length; i++) {
                        content += ` 
                            <option value="${lst[i].id}">${lst[i].name}</option>                        
                            `;
                    }
                    document.getElementById("sEditBoat").innerHTML = content;
                    axios({
                        method: "GET",
                        url: host_url + "/ticket/get_ticket",
                        headers: {
                            'Authorization': "Bearer " + token,
                            'Accept': 'application/json'
                        }
                    })
                        .then(function (res) {
                            var lst = res.data.data;
                            var content = "";
                            var boat = "";
                            var dep = "";
                            var des = "";
                            var boatlst = document.getElementById("sEditBoat");
                            var stalst = document.getElementById("sEditDeparture");
                            for (var i = 0; i < lst.length; i++) {
                                for (var j = 0; j < boatlst.length; j++) {
                                    if (boatlst.options[j].value == lst[i].schedule.boat_id) {
                                        boat = boatlst.options[j].text;
                                    }
                                }
                                for (var j = 0; j < stalst.length; j++) {
                                    if (stalst.options[j].value == lst[i].schedule.departure_station_id) {
                                        dep = stalst.options[j].text;
                                    }
                                    if (stalst.options[j].value == lst[i].schedule.arrival_station_id) {
                                        des = stalst.options[j].text;
                                    }
                                }
                                content += ` 
                                            <tr>
                                                <td>${lst[i].user.name}</td>
                                                <td>${lst[i].user.phone}</td>
                                                <td>${boat}</td>
                                                <td>${dep}</td>
                                                <td>${des}</td>
                                                <td>${lst[i].timeval}</td>
                                                <td>${lst[i].seat}</td>
                                                <td>${lst[i].pick_date}</td>
                                                <td>${lst[i].payment_method}</td>
                                                <td>${lst[i].status}</td>
                                                <td>
                                                    <a href="#" onclick="setEditData(${lst[i].id})" class="btn btn-success btn-circle btn-sm" title="Edit" data-toggle="modal" data-target="#editModal"><i class="fas fa-arrow-alt-circle-right"></i></a>
                                                </td>
                                            </tr>
                                `                   ;
                            }
                            document.getElementById("tickList").innerHTML = content;
                            var dtable = $('#dataTable').DataTable();
                        })
                        .catch(function (error) {
                            console.log(error);
                            alert("Maybe your session is expired. You should login again.");
                        });
                })
                .catch(function (error) {
                    console.log(error);
                    alert("Maybe your session is expired. You should login again.");
                });
        })
        .catch(function (error) {
            console.log(error);
            alert("Maybe your session is expired. You should login again.");
        });
});
