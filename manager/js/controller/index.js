$(document).ready(function() {
    var token = localStorage.getItem("token");
    var host_url = localStorage.getItem("host_url");
    axios({
        method: "GET",
        url: host_url +"/getdashboardinfo",
        headers: {
            'Authorization': "Bearer " + token,
            'Accept': 'application/json'
        }
    })
        .then(function (res) {
            var lst = res.data.data;
            console.log(res.data);
            
            document.getElementById("earnmonthly").innerHTML = res.data['earn_monthly'];
            document.getElementById("mosttrip").innerHTML = res.data['most_schedule'];
            document.getElementById("leasttrip").innerHTML = res.data['least_schedule'];
            document.getElementById("userNo").innerHTML = res.data['user_number'];
        })
        .catch(function (error) {
            console.log(error);
            alert("Maybe your session is expired. You should login again.");
        });
});
