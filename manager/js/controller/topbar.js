function logout() {
    localStorage.clear();
    window.location.assign("index.html");
}

$(document).ready(function() {
    var token = localStorage.getItem("token");
    var name = localStorage.getItem("username");
    var role = localStorage.getItem("role");
    if (token != "" && token != "undefined" && token != null){        
        if (role != 'admin' && role != 'operator'){
            window.location.assign("login.html");
        }
        document.getElementById("user_name").innerHTML = name;
    } else{
        window.location.assign("login.html");
    }
});

