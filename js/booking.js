var noOfSeats = 0,
    clickCounter = 0,
    k = 0,
    UserCount = 0;
$(document).ready(function () {
    var token = localStorage.getItem("token");
    var host_url = localStorage.getItem("host_url");
    var val = "all";
    if (token == "" || token == "undefined" || token == null) {
        alert("You should login for booking.");
        window.location.assign("login.html");
    }
    axios({
        method: "GET",
        url: host_url + "/searchstation",
        headers: {
            'Authorization': "Bearer " + token,
            'Accept': 'application/json'
        },
        params: {
            val: val
        }
    })
        .then(function (res) {
            var lst = res.data;
            var content = "";
            for (var i = 0; i < lst.length; i++) {
                if (lst[i].active == true) {
                    content += ` 
                    <option value="${lst[i].id}">${lst[i].station_name + " - " + lst[i].location.loc_name}</option>                        
                    `;
                }
            }
            document.getElementById("sDeparture").innerHTML = document.getElementById("sDeparture").innerHTML + content;
            document.getElementById("sArrival").innerHTML = document.getElementById("sArrival").innerHTML + content;
        })
        .catch(function (error) {
            console.log(error);
            alert("Maybe your session is expired. You should login again.");
        });

    $('.table').attr('disabled', true);
    var TableRows = $('.table tr');
    var emptyCell = '<td></td>';
    for (var i = 1; i < TableRows.length; i++) {
        var rowID = 1;
        var colId = $('.table tbody tr:nth-child(' + i + ') td:nth-child(' + 1 + ')').text();
        for (var j = 0; j < 9; j++) {
            var id = rowID + colId;
            var appendString = '<td><span class="tdBox" id=' + id + '></span></td>';
            if (j == 4) {
                $('.table tbody tr:nth-child(' + i + ')').append(emptyCell);
                rowID--;
            } else {
                $('.table tbody tr:nth-child(' + i + ')').append(appendString);
            }
            rowID++;
        }
    }

    $('#Seats').focusout(function () {
        var BookedSeats = $('#Seats').val();
        noOfSeats = BookedSeats;
        if (BookedSeats > 30) {
            alert('Please select max of 50 seats only');
            $('#Seats').val('');
            $('.table tbody tr td span').css({ opacity: 0.4 });
        }
    });
    var redCount = 0;
    $('#Selectseat').unbind('click').bind('click', function () {
        if ($('#Seats').val() != '' || $('#Seats').val() != 0) {
            var table = document.getElementById("seatTable");
            for (var i = 1, row; row = table.rows[i]; i++) {
                for (var j = 1, col; col = row.cells[j]; j++) {
                    if (col.firstChild != null) {
                        var ClassName = col.firstChild.className;
                        if (ClassName != '' && typeof ClassName !== "undefined" && ClassName !== null) {
                            if (ClassName[1] == 'redColor') {
                                redCount++;
                            } else {
                                break;
                            }
                        }
                    }
                }
            }
            if (redCount == 200) {
                $('.table tbody tr td').unbind('click');
                $('.table tbody tr td span').css({ opacity: 0.4 });
                alert('Sorry HouseFull');
                return;
            }
            $('.table').attr('disabled', false);
            $('.table tbody tr td span').css({ opacity: 1 });
            $('.table tbody tr td').unbind('click').bind('click', function () {
                var ClassName = $(this).find('span').attr('class');
                ClassName = ClassName.split(" ");
                if (ClassName[1] == 'greenColor') {
                    $(this).find('span').removeClass('greenColor');
                    clickCounter--;
                } else {
                    if (clickCounter >= noOfSeats) {
                        return;
                    } else {
                        if (ClassName[1] == 'redColor') {
                            return;
                        } else {
                            $(this).find('span').addClass('greenColor');
                            clickCounter++;
                        }
                    }
                }
            });
        } else {
            alert('Please select no of seats');
        }
    });
    $('#ConfirmSeat').unbind('click').bind('click', function () {
        var table = document.getElementById("seatTable");
        var idList = '';        
        if ($('#Seats').val() == '' || $('#Seats').val() == 0) {
            alert('Please enter number of seats to confirm');
            return;
        } else {
            for (var i = 1, row; row = table.rows[i]; i++) {
                for (var j = 1, col; col = row.cells[j]; j++) {
                    if (col.firstChild != null) {
                        var ClassName = col.firstChild.className;
                        if (ClassName != '' && typeof ClassName !== "undefined" && ClassName !== null) {
                            ClassName = ClassName.split(" ");
                            if (ClassName[1] == 'greenColor') {
                                UserCount++;
                                idList += $('.table tbody tr:nth-child(' + i + ') td:nth-child(' + (j + 1) + ') span').attr('id') + ',';
                                $('.table tbody tr:nth-child(' + i + ') td:nth-child(' + (j + 1) + ') span').removeClass('greenColor').addClass('redColor');
                            }
                            if (ClassName[1] == 'greenColor') {
                                redCount++;
                            }
                        }
                    }
                }
            }
            if (UserCount != parseInt(noOfSeats)) {
                UserCount = 0;
                var array = idList.split(',');
                for (var l = 0; l < array.length; l++) {
                    $('#' + array[l]).removeClass('redColor').addClass('greenColor');
                }
                alert('selected seats does not match with the number of seats specified');
                return;
            }
            idList = idList.substring(0, idList.length - 1);
            var newRow = document.createElement('tr');
            newRow.setAttribute('id', 'id_' + k);
            $('.resultTable').append(newRow);
            var td = document.createElement('td');
            td.innerHTML = UserCount;
            document.getElementById('id_' + k).appendChild(td);
            td = document.createElement('td');
            td.innerHTML = idList;
            document.getElementById('id_' + k).appendChild(td);
            td = document.createElement('td');
            td.innerHTML = parseInt(document.getElementById("hdPrice").value) * UserCount;
            document.getElementById('id_' + k).appendChild(td);
            document.getElementById("hdSeatLst").value = idList;
            document.getElementById("hdTotal").value = parseInt(document.getElementById("hdPrice").value) * UserCount;
            k++;
            clickCounter = 0;
            UserCount = 0;
            $('#Name').val('');
            $('#Seats').val('');
            $('.table tbody tr td').unbind('click');
            $('.table tbody tr td span').css({ opacity: 0.4 });
        }
    });

});

function searchTime() {
    const departure = document.getElementById("sDeparture").value;
    const arrival = document.getElementById("sArrival").value;
    var dt = document.getElementById("startDate").value;
    var date_pick = new Date(dt);
    var dow = date_pick.getDay();
    var token = localStorage.getItem("token");
    var host_url = localStorage.getItem("host_url");
    axios({
        method: "GET",
        url: host_url + "/schedule/find_schedule",
        headers: {
            'Authorization': "Bearer " + token,
            'Accept': 'application/json'
        },
        params: {
            departure: departure,
            arrival: arrival,
            dow: dow
        }
    })
        .then(function (res) {
            var lst = res.data;
            var content = "";
            if (lst.length > 0) {
                document.getElementById("hdPrice").value = lst[0].price;
                content = `<thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Boat</th>
                                    <th scope="col">Travel Time</th>
                                    <th scope="col">Price</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>`
            }
            let sum = 1;
            for (var i = 0; i < lst.length; i++) {
                var times = lst[i].timevals.split(",");                
                times.forEach(element => {
                    content += ` 
                            <tr>
                                <td>${sum}</td>
                                <td>${lst[i].boat.name}</td>
                                <td>${element}</td>
                                <td>${lst[i].price}</td>
                                <td>
                                    <a href="#" onclick="chooseTime(${lst[i].id}, '${element}', '${dt}')" class="btn btn-warning" title="Choose Time" >Choose</a>
                                </td>
                            </tr>
                            `;
                            sum++;
                });
            }
            document.getElementById("schelist").innerHTML = content + "</tbody>";
        })
        .catch(function (error) {
            console.log(error);
            alert("Error: " + error);
        });

    $("#editForm").validate(
        { submitHandler: function (form) { } }
    );
}

function chooseTime(sche_idx, timeval, date_pick) {    
    var token = localStorage.getItem("token");
    var host_url = localStorage.getItem("host_url");
    $.ajax({
        method: "GET",
        url: host_url + "/ticket/find_ticket",
        headers: {
            'Authorization': "Bearer " + token,
            'Accept': 'application/json'
        },
        data: {
            schedule_id: sche_idx,
            timeval: timeval,
            pick_date: date_pick
        }
      }).done(function(res) {
        $('.container-fluid').removeClass("hidden");
        document.getElementById("hdSchedule").value = sche_idx;
        document.getElementById("hdTime").value = timeval;
        document.getElementById("hdDate").value = date_pick;
        for (var i = 0; i < res.length; i++) {
            var getSeats = res[i].seat.split(",");
            getSeats.forEach(element => {
                document.getElementById(element).className = "tdBox redColor";
            });
        }
      });
}

function checkout(method) {
    var uid = localStorage.getItem("userid");
    var scheduleid = document.getElementById("hdSchedule").value;
    var timeval  = document.getElementById("hdTime").value;
    var datepick = document.getElementById("hdDate").value;
    var seats  = document.getElementById("hdSeatLst").value;
    var price = document.getElementById("hdTotal").value;
    var token = localStorage.getItem("token");
    var host_url = localStorage.getItem("host_url");
    axios({
        method: "POST",
        url: host_url +"/ticket/create_ticket",
        headers: {
            'Authorization': "Bearer " + token,
            'Accept': 'application/json'
        },
        data: {
            user_id: uid,
            schedule_id: scheduleid,
            seat: seats,
            timeval: timeval,
            pick_date: datepick,
            payment_method: method,
            price: price
        }
    })
        .then(function (res) {
            if (res.data.payment_url == ''){
                alert("Book successfully.")
                location.reload();
            } else{
                window.location.assign(res.data.payment_url);
            }            
        })
        .catch(function (error) {
            console.log(error);
            alert("Error: " + error);
        });

}