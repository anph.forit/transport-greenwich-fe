$(document).on("submit", "#signupForm", btnSignupSubmit);
function btnSignupSubmit(e) {
    e.preventDefault();

    const name = document.getElementById("txtFirstName").value + " " + document.getElementById("txtLastName").value;
    const email = document.getElementById("txtEmail").value;
    const phone = document.getElementById("txtPhone").value;
    const pass = document.getElementById("txtPassword").value;
    const rppass = document.getElementById("txtrpPassword").value;
    if (pass != rppass){
        alert("Password not match.")
        return;
    }
    const birth = document.getElementById("txtBirth").value;
    var gender = "";
    if ($('#male').is(":checked")){
        gender = "Male";
    } else{
        gender = "Female";
    }
    var host_url = localStorage.getItem("host_url");
    axios({
        method: "POST",
        url: host_url +"/register",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Type': 'application/json',
        },
        data: {
            email: email,
            password: pass,
            name: name,
            gender: gender,
            birth: birth,
            phone: phone,
        }
    })
        .then(function (res) {
            alert("Sign up successfully.");
            window.location.assign("login.html");
        })
        .catch(function (error) {
            alert("Error: " + error);
            console.log(error);
            location.reload();
        });

    $("#loginForm").validate(
        { submitHandler: function(form) {} }
    );
}

$(document).ready(function() {
    const host_url = $.getJSON( "../js/controller/config.json", function( json ) {
        localStorage.setItem("host_url", json.server_host);
    });
});
