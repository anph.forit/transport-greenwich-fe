$(document).ready(function () {
    var userid = localStorage.getItem("userid");
    var token = localStorage.getItem("token");
    var host_url = localStorage.getItem("host_url");
    axios({
        method: "GET",
        url: host_url + "/searchstation",
        headers: {
            'Authorization': "Bearer " + token,
            'Accept': 'application/json'
        },
        params: {
            val: "all"
        }
    })
        .then(function (res) {
            var lst = res.data;
            var cont = "";
            lst.forEach(element => {
                cont += `<option value="${element.id}">${element.station_name + " - " + element.location.loc_name}</option>`
            });
            document.getElementById("loclst").innerHTML = cont;
            axios({
                method: "GET",
                url: host_url + "/ticket/find_ticket",
                headers: {
                    'Authorization': "Bearer " + token,
                    'Accept': 'application/json'
                },
                params: {
                    user_id: userid
                }
            })
                .then(function (res) {
                    var lst = res.data;
                    console.log(res.data);
                    var content = "";
                    if (lst.length > 0) {
                        content = `<thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Date</th>
                                            <th scope="col">Departure</th>
                                            <th scope="col">Destination</th>
                                            <th scope="col">Seats</th>
                                            <th scope="col">Travel Time</th>
                                            <th scope="col">Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>`;

                    }
                    var dep = "";
                    var des = "";
                    var locselect = document.getElementById("loclst")                    
                    for (var i = 0; i < lst.length; i++) {
                        for (j = 0; j < locselect.length; j++) {
                            if (locselect.options[j].value == lst[i].schedule.departure_station_id){
                                dep = locselect.options[j].text;
                            }
                            if (locselect.options[j].value == lst[i].schedule.arrival_station_id){
                                des = locselect.options[j].text;
                            }
                        }
                        content += ` 
                                    <tr>
                                        <td>${i + 1}</td>
                                        <td>${lst[i].pick_date}</td>
                                        <td>${dep}</td>
                                        <td>${des}</td>
                                        <td>${lst[i].seat}</td>
                                        <td>${lst[i].timeval}</td>
                                        <td>${lst[i].price}</td>
                                    </tr>
                                    `;
                    }
                    document.getElementById("ticketlist").innerHTML = content + "</tbody>";
                })
                .catch(function (error) {
                    console.log(error);
                    alert("Error: " + error);
                });
        })
        .catch(function (error) {
            console.log(error);
            alert("Error: " + error);
        });

});